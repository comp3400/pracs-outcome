import Prelude hiding (sum, (++), reverse)

data Optional a = Empty | Full a deriving Show

mapOptional :: (a -> b) -> Optional a -> Optional b
mapOptional func optl =
  case optl of
    Empty -> error "todo"
    Full x -> error "todo"

bindOptional :: (a -> Optional b) -> Optional a -> Optional b
bindOptional = error "todo"

data List a = Nil | Cons a (List a) deriving (Eq, Show)

-- adds up the numbers in a list
sum :: List Integer -> Integer
{-
sum Nil = 0
sum (Cons h t) = h + sum t
-}
sum = foldRight (+) 0
{-
headd :: List a -> a
headd (Cons h t) = h
headd Nil = error "have a nice day"
-}

taill :: List a -> List a
taill (Cons _ t) = t
taiil x = x

data NonEmptyList a = NonEmptyList a (List a)

headdd :: NonEmptyList a -> a
headdd (NonEmptyList h _) = h

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil = b
foldRight f b (Cons h t) = f h (foldRight f b t)

-- list = Cons a (Cons b (Cons c Nil))
-- foldRight f z list = f a (f b (f c z))


-- t :: List Integer
-- h :: Integer
-- sum :: List Integer -> Integer
-- sum t :: Integer
-- _ :: Integer
-- appends two lists
(++) :: List x -> List x -> List x
(++) x y = foldRight Cons y x
{-
(++) Nil y = y
(++) (Cons h t) y = Cons h (t ++ y)
-}

-- flattens a list of lists
flatten :: List (List x) -> List x
flatten = foldRight (++) Nil

{-
flatten Nil = Nil
-- h :: List x
-- t :: List (List x)
flatten (Cons h t) = 
  h ++ flatten t
-}

-- join :: f (f a) -> f a
blah :: (t -> t -> a) -> (t -> a)
blah = \k -> \t -> k t t


bottom = bottom

constant :: a -> b -> a
constant a b = a

-- reverses a list
reverse :: List x -> List x
reverse = reverse0 Nil

foldLeft :: (b -> a -> b) -> b -> List a -> b
foldLeft = error "not today"

{-

\r el -> r + el
flipCons = \r el -> Cons el r

foldLeft f z list =
  {
    var r = Nil
    for(el in list) {
      r = flipCons(r, el)
    }
    return r
  }

-}


{-
reverse Nil = Nil
reverse (Cons h t) = (++) (reverse t) (Cons h Nil)
-}

reverse0 :: List x -> List x -> List x 
reverse0 acc Nil = acc
reverse0 acc (Cons h t) = reverse0 (Cons h acc) t

data Or a b = IsA a | IsB b

modifyA :: (a -> a) -> Or a b -> Or a b
modifyA = error "todo"

getB :: Or a b -> Optional b
getB = error "todo"

data Tree a = Tree a (List (Tree a))

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree = error "todo"

applyOptional2 :: (a -> b -> c) -> (Optional a -> Optional b -> Optional c)
applyOptional2     _                Empty         _ =           Empty
applyOptional2     _                _             Empty =       Empty
applyOptional2     f               (Full a)       (Full b) =    Full (f a b)
 

-- traverseListOptional :: (a -> Optional b) -> List a -> Optional (List b)
traverseListOptional :: (a -> Optional b) -> List a -> Optional (List b)
traverseListOptional     _                   Nil =     Full Nil
traverseListOptional     f                   (Cons h t) =  
  applyOptional2 Cons (f h) (traverseListOptional f t)
                    -- h :: a 
                    -- t :: List a
                    -- f :: a -> Optional b
  -- f h ::                      Optional b
  -- traverseListOptional f t :: Optional (List b)
  -- _ ::                        Optional (List b)

traverseTreeOptional ::
  (a -> Optional b) -> Tree a -> Optional (Tree b)
traverseTreeOptional f (Tree r c) =
  error "todo"

data ParseResult x =
  ParseError String | ParseSuccess x String
data Parser x = Parser (String -> ParseResult x)

-- the error message
parseError :: ParseResult x -> Optional String
parseError = error "todo"
-- return a parser that succeeds, puts the input to the output
neutralParser :: x -> Parser x
neutralParser = error "todo"
-- a parser that consumes one character of input (if it can)
character :: Parser Char
character = error "todo"

data Ordering' = LessThan | EqualTo | GreaterThan
class Eq x => Order x where
  compare :: x -> x -> Ordering'

data Vector6 a = Vector6 a a a a a a

maximumV6 :: Order a => Vector6 a -> a
maximumV6 = error "todo"
reverseV6 :: Order a => Vector6 a -> Vector6 a
reverseV6 = error "todo"

mapList ::     (a -> b) -> List     a -> List     b
mapList = error "alreadyDone"

-- mapOptional :: (a -> b) -> Optional a -> Optional b
-- mapOptional = error "already done"

mapParser ::   (a -> b) -> Parser   a -> Parser   b
mapParser = error "already done"

mapVector6 ::  (a -> b) -> Vector6  a -> Vector6  b
mapVector6 = error "already done"

flipList ::
  List (a -> b) -> a -> List b
flipList list a =
  mapList (\func -> func a) list

flipOptional ::
  Optional (a -> b) -> a -> Optional b
flipOptional opt a =
  mapOptional (\func -> func a) opt

flipParser ::
  Parser (a -> b) -> a -> Parser b
flipParser prsr a =
  mapParser (\func -> func a) prsr

flipVector6 ::
  Vector6 (a -> b) -> a -> Vector6 b
flipVector6 v6 a =
  mapVector6 (\func -> func a) v6

-- covariant functor
class ThingsThatMap k where
  mappy :: (a -> b) -> k a -> k b

-- instance ThingsThatMap Int where
  -- mappy :: (a -> b) -> Int a -> Int b

instance ThingsThatMap List where
  mappy = mapList

instance ThingsThatMap ((->) t) where
  -- mappy :: (a -> b) -> (->) t a -> (->) t b
  -- mappy :: (a -> b) -> (t -> a) -> t -> b
  mappy =     \f       -> \tToA -> \t -> f (tToA t) 

flippy ::
  ThingsThatMap k => k (a -> b) -> a -> k b
flippy x a =
  mappy (\func -> func a) x


