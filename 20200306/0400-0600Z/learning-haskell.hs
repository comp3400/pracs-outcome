 {-# OPTIONS_GHC -Wall #-}

x :: Integer
x = 99

f :: Integer -> Integer -> Integer
f a b = (a + b) * 2

g :: (Integer -> Integer) -> Integer
-- g = \v -> v 88
g v = v 88

h :: Integer
h = g (f 77)

i :: anything -> anythingelse -> anything
i x y = x

s :: a
s = s

j = print (i 99 s)

k :: a -> a -> a
k = 
  \a b -> a
  -- \a b -> b

l :: a
l = l

m :: Integer -> a -> Integer
m = \_ _ -> 66

n :: Bool -> Bool
n = const False

data Shape =
  Rectangle Integer Integer
  | Circle Integer
  | Triangle Integer Integer Integer
  deriving (Eq, Show)

perimeter :: Shape -> Integer
perimeter = \s -> case s of
  Circle r -> r * 3 * 2
  Rectangle w h -> (w + h) * 2
  Triangle a b c -> a + b + c

perimeteragain :: Shape -> Integer
perimeteragain (Circle r) = r * 3 * 2
perimeteragain (Rectangle w h) = (w + h) * 2
perimeteragain (Triangle a b c) = a + b + c

data Three a =
  Zero | One a | Two a a | Three a a a
  deriving (Eq, Show)

mapThree :: (a -> b) -> Three a -> Three b
mapThree = \f -> \t -> case t of
  Zero -> Zero
  One a -> One (f a)
  Two a1 a2 -> Two (f a1) (f a2)
  Three a1 a2 a3 -> Three (f a1) (f a2) (f a3)

data List a =
  Nil | Cons a (List a)
  deriving (Eq, Show)

-- headOr 33 Nil == 33
-- headOr 33 (Cons 1 (Cons 2 Nil)) == 1
headOr :: a -> List a -> a
headOr x Nil = x
headOr _ (Cons h _) = h

mapList :: (a -> b) -> List a -> List b
mapList _ Nil = Nil
mapList f (Cons h t) = 
  Cons (f h) (mapList f t)
  


