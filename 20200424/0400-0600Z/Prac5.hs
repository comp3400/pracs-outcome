{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE KindSignatures #-}

{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

module Prac5 where

import Prelude (error, (-), (++), (.), undefined)
import Control.Applicative (Applicative(..))
import Control.Monad (Monad(..))
import Data.Bool
import Data.Eq
import Data.Functor
import Data.Int
import Data.Ord
import System.IO (IO, putStrLn)
import Text.Show

import Test.Framework

{- PRELUDE -}


data List a = Nil | Cons a (List a)
  deriving (Eq, Show)
infixr 5 `Cons`

instance Functor List where
  fmap _ Nil = Nil
  fmap f (x `Cons` xs) = f x `Cons` fmap f xs

instance Applicative List where
  pure a = a `Cons` Nil
  Cons f fs <*> l2 = (fmap f l2) <> (fs <*> l2)
  Nil <*> _ = Nil

instance Monad List where
  Nil >>= _ = Nil
  Cons a as >>= f = f a <> (as >>= f)

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil      = b
foldRight f b (h `Cons` t) = f h (foldRight f b t)


data Optional a = Empty | Full a
  deriving (Eq, Show)


data NonEmptyList a = NonEmptyList a (List a)
  deriving (Eq, Show)


{- GUARDS -}

clampWhen
  :: (Int -> Bool) -- ^ predicate
  -> Int -- ^ lower bound
  -> Int -- ^ upper bound
  -> Int -- ^ input
  -> Int -- ^ output
clampWhen pred lo hi x | not predHolds = x
                       | let tooLow = x < lo in tooLow = lo
                       | x > hi = hi
                       | otherwise = x
  where
  predHolds = pred x

-- | Return the Full value iff predicate holds,
-- otherwise return default value
fullIf :: (a -> Bool) -> a -> Optional a -> a
fullIf pred _   (Full a) | pred a = a
fullIf _    def _                 = def

    

{- TOTAL AND NON-TOTAL FUNCTIONS -}

-- | Get the head of the list
--
-- What is wrong with this type?
--
{-
listHead :: List a -> a
listHead (h `Cons` _t) = h

prop_listHeadAnonFmap :: List a -> Int -> Bool
prop_listHeadAnonFmap xs n = listHead (n <$ xs) == n
-}

-- | This is a better type for 'head'.
head :: List a -> Optional a
head (h `Cons` _t) = Full h
head Nil = Empty

-- | A NonEmptyList always has a head, so we don't
-- need 'Optional'.
nehead :: NonEmptyList a -> a
nehead (NonEmptyList h _) = h

{- Ways functions can be non-total -}

-- infinite recursion
infRec :: a -> a
infRec a = infRec a
-- infRec = infRec  -- equivalent (eta-reduction)

-- non-exhaustive patterns: see listHead above

-- irrefutable patterns
listHead',listHead'',listHead''' :: List a -> a
listHead' l = let h `Cons` _t = l in h

-- 'error' and 'undefined'
listHead'' (h `Cons` _t) = h
listHead'' Nil = error "list is empty"

listHead''' (h `Cons` _t) = h
listHead''' Nil = undefined


findTwoInARow :: (Eq a) => List a -> Optional a
findTwoInARow (a `Cons` b `Cons` t)
  | a == b = Full a
  | otherwise = findTwoInARow (b `Cons` t)
findTwoInARow _ = Empty

listProgram :: (Eq a, Show a) => List a -> IO ()
listProgram l = case findTwoInARow l of
  Full a -> putStrLn ("Two in a row: " ++ show a)
  Empty -> putStrLn "There aren't two in a row"


{- MORE DATA TYPES: Identity -}

data Identity a = Identity { getIdentity :: a }
  deriving (Show)

{- record syntax above is *almost* the same as this: 

data Identity a = Identity a

getIdentity :: Identity a -> a
getIdentity (Identity a) = a

-}

instance Functor Identity where
  fmap :: (a -> b) -> Identity a -> Identity b
  fmap f (Identity a) = Identity (f a)

instance Applicative Identity where
  pure :: a -> Identity a
  pure = Identity

  (<*>) :: Identity (a -> b) -> Identity a -> Identity b
  Identity f <*> Identity a = Identity (f a)

instance Monad Identity where
  (>>=) :: Identity a -> (a -> Identity b) -> Identity b
  Identity a >>= f = f a


{- MORE DATA TYPES: Tagged -}

data Tagged a b = Tagged { getTagged :: b }

-- One possible use case for Tagged
data Validated
data NotYetValidated

type Coordinates = (Int, Int, Int)
readCoordinates :: IO (Tagged NotYetValidated Coordinates)
readCoordinates = error "todo"

validate
  :: Tagged NotYetValidated Coordinates
  -> Optional (Tagged Validated Coordinates)
validate = error "todo"
  -- check that we're not gonna nuke ourselves

launchMissilesToCoords :: Tagged Validated Coordinates -> IO ()
launchMissilesToCoords = error "todo"

instance Functor (Tagged z) where
  fmap :: (a -> b) -> Tagged z a -> Tagged z b
  fmap f (Tagged a) = Tagged (f a)

instance Applicative (Tagged z) where
  pure :: a -> Tagged z a
  pure = Tagged

  (<*>) :: Tagged z (a -> b) -> Tagged z a -> Tagged z b
  Tagged f <*> Tagged a = Tagged (f a)

-- Exercise: can you instance Monad for 'Tagged z'?
--
-- (Yes we can)
instance Monad (Tagged z) where
  (>>=) :: Tagged z a -> (a -> Tagged z b) -> Tagged z b
  Tagged a >>= f = f a


{- MORE DATA TYPES: Const -}

data Const c a = Const { getConst :: c }

instance Functor (Const c) where
  fmap :: (a -> b) -> Const c a -> Const c b
  fmap _f (Const c) = Const c

-- Exercise: can we implement Applicative?
-- If not, why not?
-- What abstraction(s) would we need to do it?
-- What laws should that abstraction obey?
--
{-
instance Applicative (Const c) where
  pure :: a -> Const c a
  pure _a = Const
    (error "we need a way to make a 'c' out of nothing")

  (<*>) :: Const c (a -> b) -> Const c a -> Const c b
  Const _c <*> Const _c' = Const
    (error "we need way to choose between c and c' OR combine them")
    -- laws of "combining"
    -- - associativity???
    -- - commutativity???
-}

{- MORE DATA TYPES: Compose -}

data Compose (f :: * -> *) (g :: * -> *) (a :: *)
  = Compose { getCompose :: f (g a) }

instance (Functor f, Functor g) => Functor (Compose f g) where
  fmap :: (a -> b) -> Compose f g a -> Compose f g b
  fmap fab (Compose fga) = Compose (fmap (fmap fab) fga)

instance (Applicative f, Applicative g) => Applicative (Compose f g) where
  pure :: a -> Compose f g a
  pure a = Compose (pure (pure a))

  (<*>) :: Compose f g (a -> b) -> Compose f g a -> Compose f g b
  Compose f <*> Compose a = Compose (liftA2 (<*>) f a)

-- Exercise: try and write a Monad instance.
-- If you cannot, why not?
instance (Monad f, Monad g) => Monad (Compose f g) where
  (>>=) :: Compose f g a -> (a -> Compose f g b) -> Compose f g b
  fga >>= f = (error "no general implementation") (fmap f fga)


{- MONOID -}

class Monoid' a where
  mempty' :: a
  mappend' :: a -> a -> a

class Semigroup (a :: *) where
  (<>) :: a -> a -> a

class (Semigroup a) => Monoid a where
  mempty :: a

mappend :: (Semigroup a) => a -> a -> a
mappend = (<>)

instance Semigroup (List a) where
  (<>) :: List a -> List a -> List a
  l1 <> l2 = foldRight Cons l2 l1

instance Monoid (List a) where
  mempty :: List a
  mempty = Nil

-- LAWS!

replicateA :: (Applicative k) => Int -> k a -> k (List a)
replicateA n k
  | n > 0 = liftA2 Cons k (replicateA (n - 1) k)
  | otherwise = pure Nil

-- useful:
-- sized :: Integral i => (i -> Gen a) -> Gen a

instance Arbitrary a => Arbitrary (List a) where
  arbitrary = sized (\len -> replicateA len arbitrary)

-- associativitiy
prop_semigroup_assoc_List
  :: List Int -> List Int -> List Int -> Bool
prop_semigroup_assoc_List l1 l2 l3 =
  (l1 <> l2) <> l3 == l1 <> (l2 <> l3)

-- left identity
prop_monoid_id_left_List :: List Int -> Bool
prop_monoid_id_left_List l = mempty <> l == l

-- right identity
prop_monoid_id_right_List :: List Int -> Bool
prop_monoid_id_right_List l = l <> mempty == l

{- Thought exercises:

* Monoids and semigroups are everywhere.  What types can you
  think of that have an associative combining function?  What
  types additionally have an identity element?

* What types /in this module/ could have lawful instances?
  You could have a go at implementing them, and writing tests.

* What types are Semigroup but not Monoid?

* Monoid is a subclass of Semigroup.  Can you think of other
  operations (with associated laws) that could further extend
  this abstraction heirarchy?

-}


-- Now we can write Applicative for 'Const c'
instance (Monoid c) => Applicative (Const c) where
  pure :: a -> Const c a
  pure _a = Const mempty

  (<*>) :: Const c (a -> b) -> Const c a -> Const c b
  Const c1 <*> Const c2 = Const (c1 <> c2)
