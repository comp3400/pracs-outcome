{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE RankNTypes #-}

{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

module Prac8 where

import Prelude ((.), ($), const, id, flip, error, Integer)

import Control.Applicative (Applicative(..))
import Data.Bool
import Data.Eq
import Data.Foldable
import Data.Functor
import Data.Functor.Const
import Data.List (reverse)
import Data.Maybe
import Data.Monoid (Monoid(..), First(..))
import Data.Semigroup (Semigroup(..))
import Data.Ord
import Data.String (String)
import Data.Traversable
import Text.Show

{-# ANN module "HLint: ignore" #-}

{- PRELUDE -}

newtype Identity a = Identity { getIdentity :: a }
  deriving (Show)

instance Functor Identity where
  fmap :: (a -> b) -> Identity a -> Identity b
  fmap f (Identity a) = Identity (f a)

instance Applicative Identity where
  pure :: a -> Identity a
  pure = Identity

  (<*>) :: Identity (a -> b) -> Identity a -> Identity b
  Identity f <*> Identity a = Identity (f a)

data List a = Nil | Cons a (List a)
  deriving (Eq, Show)
infixr 5 `Cons`

foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil      = b
foldRight f b (h `Cons` t) = f h (foldRight f b t)


{- REVIEW: Traversal -}

type Traversal s t a b =
  forall k. (Applicative k)
  => (a -> k b) -> s -> k t

type Traversal' s a = Traversal s s a a


-- | Traversal for **both sides** of a pair
both :: Traversal (a, a) (b, b) a b
both f (a1, a2) = liftA2 (,) (f a1) (f a2)

-- | A Traversal over any 'Traversable t => t a'
traversed
  :: (Traversable t)
  => Traversal (t a) (t b) a b
traversed = traverse

-- | Obtain a Traversal that can be composed with
-- another optic to "filter" it.
--
-- This is not a lawful Traversal in general.
-- You must take care not to invalidate the predicate!
--
filtered :: (a -> Bool) -> Traversal a a a a
filtered pred f a
  | pred a = f a
  | otherwise = pure a


{- REVIEW: Setter -}

type Setter s t a b =
  (a -> Identity b) -> s -> Identity t

-- | Modify the target(s) of a traversal
over :: Setter s t a b -> (a -> b) -> s -> t
over l f s = getIdentity ( l (Identity . f) s )

-- | Set the target(s) of a traversal
set :: Setter s t a b -> b -> s -> t
set l = over l . const
-- set l b = over l (const b)
-- set l b s = over l (const b) s

{- REVIEW: Fold -}

type Fold s a =
  forall r. (Monoid r)
  => (a -> Const r a) -> s -> Const r s

-- | A Fold over any 'Foldable t => t a'
folded :: (Foldable t) => Fold (t a) a
folded f = Const . foldMap (getConst . f)

-- If we specialise foldMapOf's functor to 'Const r' we can
-- avoid the Monoid constraint here
foldMapOf
  :: ((a -> Const r b) -> s -> Const r t)
  -> (a -> r)
  -> s
  -> r
foldMapOf l f = getConst . l (Const . f)

-- We can extract this type to a type synonym:
type Getting r s a = (a -> Const r a) -> s -> Const r s

foldOf :: Getting a s a -> s -> a
foldOf l = getConst . l Const

firstOf :: Getting (First a) s a -> s -> Maybe a
firstOf l = getFirst . getConst . l (Const . First . Just)


{- REVIEW: Lens -}

type Lens s t a b =
  forall k. (Functor k)
  => (a -> k b) -> s -> k t

type Lens' s a = Lens s s a a

fstL :: Lens (a, x) (b, x) a b
fstL f (a, x) = (\b -> (b, x)) <$> f a

sndL :: Lens (x, a) (x, b) a b
sndL f (x, a) = (\b -> (x, b)) <$> f a

view :: Getting a s a -> s -> a
view = foldOf


{- EXERCISE: Endo -}

newtype Endo a = Endo { appEndo :: a -> a }

instance Semigroup (Endo a) where
  Endo g <> Endo f = Endo (g . f)

instance Monoid (Endo a) where
  mempty = Endo id

foldrOf
  :: Getting (Endo r) s a
  -> (a -> r -> r)
  -> r
  -> s
  -> r
foldrOf l f z =
  (\endo -> appEndo endo z) . getConst
  . l (Const . Endo . f)

toListOf :: Getting (Endo [a]) s a -> s -> [a]
toListOf l = foldrOf l (:) []


{- EXERCISE: Profunctor -}

-- | Map contravariantly on the first type parameter,
--   and covariantly on the second type parameter.
class Profunctor (p :: * -> * -> *) where
  dimap :: (b -> a) -> (c -> d) -> p a c -> p b d

instance Profunctor (->) where
  dimap :: (b -> a) -> (c -> d) -> (a -> c) -> (b -> d)
  dimap i o f = o . f . i


newtype Tagged x a = Tagged { unTagged :: a }

instance Functor (Tagged x) where
  fmap :: (a -> b) -> Tagged x a -> Tagged x b
  fmap f (Tagged a) = Tagged (f a)

instance Applicative (Tagged x) where
  pure :: a -> Tagged x a
  pure = Tagged

  (<*>) :: Tagged x (a -> b) -> Tagged x a -> Tagged x b
  Tagged f <*> Tagged a = Tagged (f a)

instance Profunctor Tagged where
  dimap
    :: (b -> a) -> (c -> d)
    -> Tagged a c -> Tagged b d
  dimap _l r (Tagged c) = Tagged (r c)


{- EXERCISE: Choice -}

data Either a b = Left a | Right b
  deriving (Eq, Show)

instance Functor (Either a) where
  fmap f e = case e of
    Left a -> Left a
    Right b -> Right (f b)

-- Either has kind * -> * -> *;
-- so can we instance Profunctor Either?
instance Profunctor Either where
  dimap _l r (Right b) = Right (r b)
  dimap _l _r (Left _a) = Left (error "struck")

-- The 'a' in 'Profunctor p => p a b' must be
-- CONTRAvariant.

-- "inline" case analysis for 'Either'
either :: (a -> r) -> (b -> r) -> Either a b -> r
either l r e = case e of
  Left a -> l a
  Right b -> r b

-- | Turn a "plain" profunctor into a profunctor that
-- maps the left or right side of an Either
class (Profunctor p) => Choice p where
  left  :: p a b -> p (Either a c) (Either b c)
  right :: p a b -> p (Either c a) (Either c b)

instance Choice (->) where
  left :: (a -> b) -> (Either a c -> Either b c)
  left f = either (Left . f) Right

  right :: (a -> b) -> (Either c a -> Either c b)
  right = fmap

instance Choice Tagged where
  left :: Tagged a b -> Tagged (Either a c) (Either b c)
  left (Tagged b) = Tagged (Left b)

  right :: Tagged a b -> Tagged (Either c a) (Either c b)
  right (Tagged b) = Tagged (Right b)


{- EXERCISE: Prism -}

-- | Prism generalises Traversal,
-- replacing '(->)' with '(Choice p) => p'
--
type Prism s t a b =
  forall p k. (Choice p, Applicative k)
  => p a (k b) -> p s (k t)

type Prism' s a = Prism s s a a

-- | Prism to Left constructor
_Left :: Prism (Either a x) (Either b x) a b
_Left =
  dimap id (either (fmap Left) (pure . Right)) . left

-- | Prism to Left constructor
_Right :: Prism (Either x a) (Either x b) a b
_Right =
  dimap id (either (pure . Left) (fmap Right)) . right

-- Construct a prism from a constructor and "partial"
-- getter
prism :: (b -> t) -> (s -> Either t a) -> Prism s t a b
prism bt seta =
  dimap seta (either pure (fmap bt)) . right

-- | Prism to Just constructor
_Just :: Prism (Maybe a) (Maybe b) a b
_Just = prism Just
  (\x -> case x of Just a -> Right a ; _ -> Left Nothing)

-- | Prism to Nothing constructor
_Nothing :: Prism (Maybe a) (Maybe a) () ()
_Nothing = prism
  (const Nothing)
  (\x -> case x of Nothing -> Right () ; ja -> Left ja)

{- Composing Prisms

What is the type of a Prism composed with...

* another Prism?
   answer: Prism
* a Lens?
   answer: Traversal
* a Traversal?
   answer: Traversal
* a Fold?
   answer: Fold

Use GHCi to check.  Why does each composition have the
type it does?

-}

{- EXERCISE: Iso[morphism] -}

-- | We can relax Choice to Profunctor, and Applicative
--   to Functor, we get an Isomorphism
type Iso s t a b =
  forall p k. (Profunctor p, Functor k)
  => p a (k b) -> p s (k t)

type Iso' s a = Iso s s a a

-- | An Iso is a witness of structural isomorphism
iso :: (s -> a) -> (b -> t) -> Iso s t a b
iso f g = dimap f (fmap g)

swapped :: Iso' (a, b) (b, a)
swapped = error "todo"

reversed :: Iso' [a] [a]
reversed = iso reverse reverse

listIso :: Iso' [a] (List a)
listIso = iso (foldr Cons Nil) (foldRight (:) [])

-- | Use a Prism (or Iso) as a constructor for the
-- "large" type
--
-- **Hint** use 'Tagged' and 'Identity'
--
review :: Prism s t a b -> b -> t
review l =
  getIdentity . unTagged . l . Tagged . Identity

-- | Invert an Iso
from :: Iso s t a b -> Iso b a t s
from l = iso (review l) (getConst . l Const)

{- Composing Prisms

What is the type of an Iso composed with...

* another Iso?
* a Prism?
* a Traversal?
* a Lens?
* a Fold?

Use GHCi to check.  Why does each composition have the
type it does?

-}

-- | Iso that reverses a 'List a'
reversedList :: Iso' (List a) (List a)
reversedList = from listIso . reversed . listIso


{- EXERCISE: Application -}

type Name = String

type Address = String

data Person = Person Name Address
  deriving (Show)

personName :: Lens' Person Name
personName f (Person name addr) =
  (\name' -> Person name' addr) <$> f name

personAddress :: Lens' Person Address
personAddress f (Person name addr) =
  (\addr' -> Person name addr') <$> f addr

type ABN = String

type RegistrationNumber = String

data RegisteredTo
  = RegisteredToBusiness ABN
  | RegisteredToPerson Person
  deriving (Show)

regoABN :: Prism' RegisteredTo ABN
regoABN = prism
  RegisteredToBusiness
  ( \reg -> case reg of
      RegisteredToBusiness abn -> Right abn
      x -> Left x
  )

regoPerson :: Prism' RegisteredTo Person
regoPerson = prism
  RegisteredToPerson
  ( \reg -> case reg of
      RegisteredToPerson who -> Right who
      x -> Left x

  )

type Registration = (RegistrationNumber, RegisteredTo)

type VIN = Integer

type Make = String
type Model = String

data Vehicle = Vehicle
  VIN
  Make
  Model
  (Maybe Registration)
  deriving (Show)

vehicleVIN :: Lens' Vehicle VIN
vehicleVIN f (Vehicle vin make model reg) =
  (\vin' -> Vehicle vin' make model reg) <$> f vin

vehicleMake :: Lens' Vehicle Make
vehicleMake f (Vehicle vin make model reg) =
  (\make' -> Vehicle vin make' model reg) <$> f make

vehicleModel :: Lens' Vehicle Model
vehicleModel f (Vehicle vin make model reg) =
  (\model' -> Vehicle vin make model' reg) <$> f model

vehicleRegistration :: Lens' Vehicle (Maybe Registration)
vehicleRegistration f (Vehicle vin make model reg) =
  (\reg' -> Vehicle vin make model reg') <$> f reg

db :: [Vehicle]
db =
  [ Vehicle 1 "Lamborghini" "Sian"
      (Just ("725MJM", RegisteredToPerson alice))
  , Vehicle 2 "Ford" "Pinto"
      (Just ("692LXO", RegisteredToPerson bob))
  , Vehicle 3 "Ford" "Model T"
      (Just ("755XXH", RegisteredToBusiness "42"))
  , Vehicle 4 "Ford" "Pinto"
      Nothing
  , Vehicle 5 "Lada" "Niva"
      (Just ("363OCL", RegisteredToPerson carol))
  , Vehicle 6 "McLaren" "Senna"
      (Just ("687QYF", RegisteredToPerson alice))
  ]
  where
    alice = Person "Alice" "208 Vale Drive"
    bob = Person "Bob" "64 Brook Drive"
    carol = Person "Carol" "292 Galvin St"

-- | Register vehicle with given VIN, using the
-- specified RegistrationNumber.  If VIN is unknown
-- do nothing.
--
register
  :: VIN
  -> RegisteredTo
  -> RegistrationNumber
  -> [Vehicle]
  -> [Vehicle]
register vin who plate = set l (Just (plate, who))
  where
  l =
      traversed
      . filtered ((== vin) . view vehicleVIN)
      . vehicleRegistration

-- | Fraser has become the proud owner of a classic
-- Ford Pinto, VIN 4.
registerFraser'sPinto :: [Vehicle] -> [Vehicle]
registerFraser'sPinto =
  register 4 (RegisteredToPerson fraser) nextRego
  where
    nextRego = "757AQA"
    fraser = Person "Fraser" "7 Pierce Road"

-- | Uh oh, Ford Pintos are exploding.
--
-- We need to find the address of all registered owners
-- of Pintos, so we can send them some paperwork.
--
-- Don't worry about the vehicles registered to
-- companies.
--
pintoPeople :: [Vehicle] -> [Address]
pintoPeople = toListOf l
  where
  l =
      folded
      . filtered ((== "Ford") . view vehicleMake)
      . filtered ((== "Pinto") . view vehicleModel)
      . vehicleRegistration
      . _Just
      . sndL
      . regoPerson
      . personAddress
