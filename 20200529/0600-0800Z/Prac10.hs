{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RankNTypes #-}

module Prac10 where

import Prelude ((.), error)

import Control.Applicative
import Control.Monad
import Data.Functor
import Data.Eq
import Text.Show


{- Profunctor -}

class Profunctor (p :: * -> * -> *) where
  dimap :: (b -> a) -> (c -> d) -> p a c -> p b d

instance Profunctor (->) where
  dimap :: (b -> a) -> (c -> d) -> (a -> c) -> (b -> d)
  dimap i o f = o . f . i


newtype Tagged x a = Tagged { unTagged :: a }

instance Functor (Tagged x) where
  fmap :: (a -> b) -> Tagged x a -> Tagged x b
  fmap f (Tagged a) = Tagged (f a)

instance Applicative (Tagged x) where
  pure :: a -> Tagged x a
  pure = Tagged

  (<*>) :: Tagged x (a -> b) -> Tagged x a -> Tagged x b
  Tagged f <*> Tagged a = Tagged (f a)

instance Profunctor Tagged where
  dimap
    :: (b -> a) -> (c -> d)
    -> Tagged a c -> Tagged b d
  dimap _l r (Tagged c) = Tagged (r c)


{- Choice -}

data Either a b = Left a | Right b
  deriving (Eq, Show)

instance Functor (Either a) where
  fmap f e = case e of
    Left a -> Left a
    Right b -> Right (f b)

-- Either has kind * -> * -> *;
-- so can we instance Profunctor Either?
instance Profunctor Either where
  dimap _l r (Right b) = Right (r b)
  dimap _l _r (Left _a) = Left (error "struck")

-- The 'a' in 'Profunctor p => p a b' must be
-- CONTRAvariant.

-- "inline" case analysis for 'Either'
either :: (a -> r) -> (b -> r) -> Either a b -> r
either l r e = case e of
  Left a -> l a
  Right b -> r b

-- | Turn a "plain" profunctor into a profunctor that
-- maps the left or right side of an Either
class (Profunctor p) => Choice p where
  left  :: p a b -> p (Either a c) (Either b c)
  right :: p a b -> p (Either c a) (Either c b)

instance Choice (->) where
  left :: (a -> b) -> (Either a c -> Either b c)
  left f = either (Left . f) Right

  right :: (a -> b) -> (Either c a -> Either c b)
  right = fmap

instance Choice Tagged where
  left :: Tagged a b -> Tagged (Either a c) (Either b c)
  left (Tagged b) = Tagged (Left b)

  right :: Tagged a b -> Tagged (Either c a) (Either c b)
  right (Tagged b) = Tagged (Right b)



{- Optic types -}

type Traversal s t a b  = forall k.   (Applicative k)           => (->) a (k b) -> (->) s (k t)

type Lens s t a b       = forall k.   (Functor k)               => (->) a (k b) -> (->) s (k t)

type Prism s t a b      = forall p k. (Choice p, Applicative k) => p    a (k b) -> p    s (k t)

type Iso s t a b        = forall p k. (Profunctor p, Functor k) => p    a (k b) -> p    s (k t)

{-

       Traversal

     /          \

  Prism         Lens

     \          /

         Iso

When does each kind of optic apply?

- Iso s a: 's' contains exactly one 'a' and nothing else
  (newtypes, invertable morphisms)

- Prism' s a: 's' contains exactly one 'a' OR some other thing(s)
  (access a constructor of a sum type)

- Lens' s a: 's' contains exactly one 'a' AND some other thing(s)
  (access fields of a product/record type)

- Traversal' s a: 's' contains zero or more 'a's
  AND/OR other thing(s) OR nothing else


What are the "is a" relationships for optics?

- Iso /is a/ Lens, Prism, Traversal
- Lens /is a/ Traversal
- Prism /is a/ Traversal

-}


-- What other ways could we generalise the type of Traversal?

type WhatIsThis  s t a b = forall p k. (Choice p, Functor k)         => p    a (k b) -> p    s (k t)

type WhatIsThis' s t a b = forall p k. (Profunctor p, Applicative k) => p    a (k b) -> p    s (k t)

type WhatIsThis'' s t a b =
  forall p q k. (Profunctor p, Profunctor q, Applicative k) => q (p a (k b)) (p s (k t))
                                       -- ^ what about Choice q?
                                       -- ^ what about Strong q?
  -- Alas, generalising the "middle" (->) to 'Profunctor q => q'
  -- means we lose the use of (.) for composition.
  --
  -- (There is a way around this, but I won't go into it).
