{- Prac 9: Logic programming with LogicT

Code and exercises in this prac are derived from paper
/Backtracking, Interleaving and Terminating Monad Transformers/
(Kiselyov et al, 2005) [1] and the /logict/ library [2].

[1] http://okmij.org/ftp/papers/LogicT.pdf
[2] https://hackage.haskell.org/package/logict

I recommend reading the paper for a deeper understanding of the
motivation and implementation of these abstractions, and the laws.

-}
{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TupleSections #-}

module Logic where

import Prelude
  ( ($), (.), const, id, flip, error
  , Int, (+), (-), even, odd, mod
  )


import Data.Bool
import Data.Eq
import Data.Foldable (Foldable(..))
import Data.Functor
import Data.Functor.Identity
import Data.Maybe
import Data.Ord
import Data.Semigroup (Semigroup(..))
import Data.Monoid (Monoid(..))
import Data.Tuple
import Control.Applicative (Applicative(..), Alternative(..))
import Control.Monad (Monad(..), MonadPlus(..), (=<<), guard)




{- EXERCISE: LogicT -}

-- | This is the Church-encoded list transformer.  We will equip
--   it with special behaviour for logic programming later.
--
newtype LogicT k a = LogicT { unLogicT :: forall r. (a -> k r -> k r) -> k r -> k r }

-- Compare LogicT with (Church) List
--
newtype List     a = List   { unList   :: forall r. (a ->   r ->   r) ->   r ->   r }

-- Like other monad transformers, we recover the "plain" (non-transformed)
-- version by instantiating 'k' at Identity
--
type Logic = LogicT Identity   -- isomorphic to List


{- EXERCISE: Church-encoded list -}

nil :: LogicT k a
nil = LogicT $ \_c n -> n

cons :: a -> LogicT k a -> LogicT k a
cons a (LogicT l) = LogicT $ \c n -> c a (l c n)


instance Semigroup (LogicT k a) where
  LogicT l1 <> LogicT l2 = LogicT $ \c n -> l1 c (l2 c n)

instance Monoid (LogicT k a) where
  mempty = nil


instance Functor (LogicT k) where
  fmap :: (a -> b) -> LogicT k a -> LogicT k b
  fmap f (LogicT l) = LogicT $ \c n -> l (c . f) n

instance Applicative (LogicT k) where
  pure :: a -> LogicT k a
  pure a = cons a nil

  (<*>) :: LogicT k (a -> b) -> LogicT k a -> LogicT k b
  LogicT lf <*> LogicT la =
    LogicT $ \c n -> lf (\f r -> la (c . f) r) n

instance Monad (LogicT k) where
  (>>=) :: LogicT k a -> (a -> LogicT k b) -> LogicT k b
  LogicT l >>= f =
    LogicT $ \c n -> l (\a r -> unLogicT (f a) c r) n

instance Alternative (LogicT k) where
  empty :: LogicT k a
  empty = nil

  (<|>) :: LogicT k a -> LogicT k a -> LogicT k a
  (<|>) = (<>)

instance MonadPlus (LogicT k) where
  -- use default implementations of mzero and mplus


{- EXERCISE: Foldable LogicT -}

instance (Applicative k, Foldable k) => Foldable (LogicT k) where
  foldMap :: (Monoid m) => (a -> m) -> LogicT k a -> m
  foldMap f (LogicT l) =
    fold $ l (\a r -> fmap (f a <>) r) (pure mempty)

-- Takes a foldable of (k a) and folds using (<|>) and empty
--
-- Note: Foldable gives us 'foldr' (on t) for free
--
asum :: (Foldable t, Alternative k) => t (k a) -> k a
asum = foldr (<|>) empty


{- EXERCISE: LogicT combinators -}

lift :: (Monad k) => k a -> LogicT k a
lift ka = LogicT $ \c n -> ka >>= \a -> c a n

reflect :: (Alternative k) => Maybe (a, k a) -> k a
reflect ma = case ma of
  Nothing -> empty
  Just (a, ka) -> pure a <|> ka

msplit :: (Monad k) => LogicT k a -> LogicT k (Maybe (a, LogicT k a))
msplit (LogicT l) =
  lift $
    l
      (\a r -> pure (Just (a, lift r >>= reflect)))
      (pure Nothing)

-- | Fair disjunction
--
-- is needed because (l <|> r) will never consider results from
-- 'r' if 'l' is infinite
--
interleave :: (Monad k) => LogicT k a -> LogicT k a -> LogicT k a
interleave l r = msplit l >>= \x -> case x of
  Nothing -> r
  Just (a, l') -> pure a <|> interleave r l'

-- | Fair conjunction
(>>-) :: (Monad k) => LogicT k a -> (a -> LogicT k b) -> LogicT k b
l >>- f = msplit l >>= \x -> case x of
  Nothing -> empty 
  Just (a, l') -> interleave (f a) (l' >>- f)

-- | Logical conditional
ifte  -- IF-Then-Else
  :: (Monad k)
  => LogicT k a         -- ^ test
  -> (a -> LogicT k b)  -- ^ success branch
  -> LogicT k b         -- ^ failure branch
  -> LogicT k b
ifte test win lose =
  msplit test
  >>= maybe lose (\(a, test') -> win a <|> (test' >>= win))

-- | Pruning; select one result
once :: (Monad k) => LogicT k a -> LogicT k a
once l = msplit l >>= maybe empty (\(a, _) -> pure a)

-- | Logical negation
lnot :: (Monad k) => LogicT k a -> LogicT k ()
lnot l = ifte (once l) (const empty) (pure ())




{- EXERCISE: observing results -}

-- | Extract first result
observeT :: (Applicative k) => LogicT k a -> k (Maybe a)
observeT (LogicT l) =
  l (\a _r -> pure (Just a)) (pure Nothing)

-- | Extract all results
observeAllT :: (Applicative k) => LogicT k a -> k [a]
observeAllT (LogicT l) = l (fmap . (:)) (pure [])

-- | Extract up to /n/ results
observeManyT :: (Monad k) => Int -> LogicT k a -> k [a]
observeManyT n l
  | n <= 0 = pure []
  | otherwise =
      unLogicT (msplit l)
        (\ma r ->
          maybe
            r
            ( \(a, l') -> fmap (a:) (observeManyT (n - 1) l') )
            ma
        )
        (pure [])


{- EXERCISE: Logic -}

observe :: Logic a -> Maybe a
observe = runIdentity . observeT

observeAll :: Logic a -> [a]
observeAll = runIdentity . observeAllT

observeMany :: Int -> Logic a -> [a]
observeMany n = runIdentity . observeManyT n


{- EXERCISE: building logic computations -}

odds :: LogicT k Int
odds = pure 1 <|> fmap (+2) odds

oddsPlus :: Int -> LogicT k Int
oddsPlus x = fmap (+x) odds

zeroAndOne :: LogicT k Int
zeroAndOne = pure 0 <|> pure 1

oddsAndEvensNaïveDis :: LogicT k Int
oddsAndEvensNaïveDis = odds <|> oddsPlus 1

oddsAndEvensNaïveCon :: LogicT k Int
oddsAndEvensNaïveCon = zeroAndOne >>= oddsPlus

oddsAndEvensFairDis :: (Monad k) => LogicT k Int
oddsAndEvensFairDis = interleave odds (oddsPlus 1)

oddsAndEvensFairCon :: (Monad k) => LogicT k Int
oddsAndEvensFairCon = zeroAndOne >>- oddsPlus

-- | Return even numbers from the input
--
-- What happens if you apply to oddsAndEvensFair?
-- What happens if you apply to oddsAndEvensNaïve?
--
evensFrom :: LogicT k Int -> LogicT k Int
evensFrom l = do
  x <- l
  guard (even x)
  pure x

-- | Generate ints from 1..n
iota :: (Alternative k) => Int -> k Int
iota n = asum (fmap pure [1..n])

oddPrimes :: (Monad k) => LogicT k Int
oddPrimes = do
  x <- odds
  guard (x > 1)
  lnot $ do
    d <- iota (x - 1)  -- numbers from 1 .. x-1
    guard (d > 1)      -- exclude denominator d = 1
    guard (x `mod` d == 0)  -- succeed if d divides x
  pure x

-- use 'oddPrimes' and @pure 2@
primes :: (Monad k) => LogicT k Int
primes =
  -- oddPrimes <|> pure 2  (2 never considered)
  oddPrimes `interleave` pure 2  -- fair disjunction

-- use 'primes'
evenPrimes :: (Monad k) => LogicT k Int
evenPrimes = do
  x <- primes
  guard (even x)
  pure x


{- Logic puzzle

Which answer is correct?

a. All of the below
b. None of the below
c. All of the above
d. One of the above
e. None of the above
f. None of the above

-}

tf :: LogicT k Bool
tf = pure True <|> pure False

(→) :: Bool -> Bool -> LogicT m ()
p → q = guard (p && q || not p && not q)
infixr 1 →

(∧), (∨) :: Bool -> Bool -> Bool
(∧) = (&&)
(∨) = (||)
infixr 3 ∧
infixr 2 ∨

(¬) :: Bool -> Bool
(¬) = not

solve :: LogicT k (Bool, Bool, Bool, Bool, Bool, Bool)
solve = do
  a <- tf ; b <- tf ; c <- tf ; d <- tf ; e <- tf ; f <- tf
  a → b ∧ c ∧ d ∧ e ∧ f
  b → (¬)(c ∨ d ∨ e ∨ f)
  c → a ∧ b
  d → (a ∧ (¬)b ∧ (¬)c) ∨ ((¬)a ∧ b ∧ (¬)c) ∨ ((¬)a ∧ (¬)b ∧ c)
  e → (¬)(a ∨ b ∨ c ∨ d)
  f → (¬)(a ∨ b ∨ c ∨ d ∨ e)
  pure (a, b, c, d, e, f)
