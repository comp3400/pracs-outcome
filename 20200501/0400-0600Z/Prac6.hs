{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}

module Prac6 where

import Prelude (error, even, id, (.), ($))

import Control.Applicative (Applicative(..))
import Control.Monad (Monad(..), mapM)
import Data.Bool
import Data.Char
import Data.Eq
import Data.Foldable (notElem, elem, foldr)
import Data.Functor
import Data.Ord
import Data.Int (Int)
import Data.String (String)
import Data.Tuple (fst, snd)
import Data.Traversable (sequence)
import Text.Show

{- ASSIGNMENT 2 QUESTIONS -}

{- How to convert string to int -}

-- | convert string to int
--
-- Other considerations:
--
-- - negative numbers
stringToInt :: String -> Optional Int
stringToInt s = bogus <$> mapM charToInt s
  where
  bogus [] = 0
  bogus (x:_) = x

-- | handle a single char (decimals)
charToInt :: Char -> Optional Int
charToInt c = case c of
  '0' -> Full 0
  '1' -> Full 1
  '2' -> Full 2
  '3' -> Full 3
  '4' -> Full 4
  '5' -> Full 5
  '6' -> Full 6
  '7' -> Full 7
  '8' -> Full 8
  '9' -> Full 9
  _ -> Empty


{- ACKNOWLEDGEMENTS

The exercises in this prac are based on the NICTA/Data61/System F
FP course (https://github.com/system-f/fp-course) and in particular
the Course.Applicative and Course.StateT modules.

-}


{- PRELUDE -}

data Optional a = Empty | Full a
  deriving (Eq, Show)

instance Functor Optional where
  fmap f o = case o of
    Empty -> Empty
    Full a -> Full (f a)

instance Applicative Optional where
  pure = Full
  o1 <*> o2 = case o1 of
    Empty -> Empty
    Full f -> f <$> o2

instance Monad Optional where
  o >>= f = case o of
    Empty -> Empty
    Full a -> f a

when :: (Applicative k) => Bool -> k () -> k ()
when b k = case b of
  True -> k
  False -> pure ()


{- REVISION: Identity -}

data Identity a = Identity { getIdentity :: a }
  deriving (Show)

instance Functor Identity where
  fmap :: (a -> b) -> Identity a -> Identity b
  fmap f (Identity a) = Identity (f a)

instance Applicative Identity where
  pure :: a -> Identity a
  pure = Identity

  (<*>) :: Identity (a -> b) -> Identity a -> Identity b
  Identity f <*> Identity a = Identity (f a)

instance Monad Identity where
  (>>=) :: Identity a -> (a -> Identity b) -> Identity b
  Identity a >>= f = f a


{- REVISION: Semigroup and Monoid -}

class Semigroup (a :: *) where
  (<>) :: a -> a -> a
  -- laws: associativity
  -- (a <> b) <> c == a <> (b <> c)

class (Semigroup a) => Monoid a where
  mempty :: a
  -- laws: left and right identity
  -- mempty <> a == a
  -- a <> mempty == a
  

instance Semigroup ([] a) where
  l1 <> l2 = foldr (:) l2 l1

instance Monoid ([] a) where
  mempty = []


{- EXERCISE: Writer -}

-- Intuition: /computing/ a value of type 'a'
--   while /accumulating/ a value of type 'w'
--
-- e.g.
--
-- * count line numbers while parsing a file
-- * log messages while performing a computation
--
data Writer w a = Writer w a
  deriving (Show)

-- observe: Writer     ~ ( ,  )
--          Writer w a ~ (w, a) ~ (a, w)

instance Functor (Writer w) where
  fmap :: (a -> b) -> Writer w a -> Writer w b
  fmap f (Writer w a) = Writer w (f a)

instance (Monoid w) => Applicative (Writer w) where
  pure :: a -> Writer w a
  pure a = Writer mempty a

  (<*>) :: Writer w (a -> b) -> Writer w a -> Writer w b
  Writer w1 f <*> Writer w2 a = Writer (w1 <> w2) (f a)

instance (Monoid w) => Monad (Writer w) where
  (>>=) :: Writer w a -> (a -> Writer w b) -> Writer w b
  Writer w1 a >>= f =
    case f a of  -- we need to pattern match result of (f a)
      Writer w2 b -> Writer (w1 <> w2) b
    -- equivalent to:
    -- let Writer w2 b = f a
    -- in Writer (w1 <> w2) b

tell :: w -> Writer w ()
tell w = Writer w ()

{- Thought exercise:

* How is Writer similar to Const (from Prac 5)?
* How do they differ?

-}


{- EXERCISE: StateT ; stateful computations

You have already seen StateT in the lectures.
Let's go over it again.

-}

-- | USEFUL FUNCTION: Apply f to /first/ value in tuple
first :: (a -> b) -> (a, c) -> (b, c)
first f (a, c) = (f a, c)
-- you could also define 'second', but we don't need it today

data StateT s k a =
  StateT { runStateT :: s -> k (a, s) }

-- why "runStateT" for the getter
--  runStateT theStateComputation initialState

instance (Functor k) => Functor (StateT s k) where
  fmap :: (a -> b) -> StateT s k a -> StateT s k b
  fmap f k = StateT $ \s -> 
    first f <$> runStateT k s

instance (Monad k) => Applicative (StateT s k) where
  pure :: a -> StateT s k a
  pure a = StateT $ \s -> pure (a, s)

  (<*>) :: StateT s k (a -> b) -> StateT s k a -> StateT s k b
  kf <*> ka = StateT $ \s -> do
    (f, s') <- runStateT kf s
    (a, s'') <- runStateT ka s'
    pure (f a, s'')

instance Monad k => Monad (StateT s k) where
  (>>=) :: StateT s k a -> (a -> StateT s k b) -> StateT s k b
  ka >>= f = StateT $ \s -> do
    (a, s') <- runStateT ka s
    runStateT (f a) s'

-- | Run the StateT computation, returning the /final state/
execStateT :: (Functor k) => StateT s k a -> s -> k s
execStateT k s = snd <$> runStateT k s

-- | Run the StateT computation, returning the /result value/
evalStateT :: (Functor k) => StateT s k a -> s -> k a
evalStateT k s = fst <$> runStateT k s

-- | Get the current state
get :: (Applicative k) => StateT s k s
get = StateT $ \s -> pure (s, s)

-- | Put (set) a new state
put :: (Applicative k) => s -> StateT s k ()
put s = StateT $ \_ -> pure ((), s)

-- Observe that we recover a "pure" state computation
-- by instantiating 'k' at the "neutral" monad, i.e.
-- 'Identity'
type State s = StateT s Identity

execState :: State s a -> s -> s
execState k s =
  (snd . getIdentity) (runStateT k s)
  -- equivalent to:
  -- runIdentity (execStateT k s)

evalState :: State s a -> s -> a
evalState k = getIdentity . evalStateT k

runState :: State s a -> s -> (a, s)
runState k = getIdentity . runStateT k



-- | Filter a list with an "effectful" predicate
filtering :: (Applicative k) => (a -> k Bool) -> [a] -> k [a]
filtering pred l = case l of
  [] -> pure []
  (x:xs) ->
    liftA2
      (\b -> if b then (x:) else id)
      (pred x)
      (filtering pred xs)

-- | Remove all duplicate elements in a list.
--
-- /Tip:/ Use `filtering` and `State [a]`.
--
distinct :: (Eq a) => [a] -> [a]
distinct l =
  evalState
    (filtering test l)
    [] -- values we've seen ; initial state empty
       -- note: asymptotics using [] are terrible,
       --   in real world use use a proper Set type
       --   e.g. Data.Set.Set from /containers/ lib
  where
  test :: (Eq a) => a -> State [a] Bool
  test x = do
    seen <- get
    put (x:seen)
    pure (not (x `elem` seen))


-- | Remove all duplicate elements in a list.
-- However, if you see a value greater than `100` in the list,
-- abort the computation by producing `Empty`.
--
-- /Tip:/ Use `filtering` and `StateT [Int] Optional`
--
-- >>> distinctMax100 [1,2,3,2,1]
-- Full [1,2,3]
--
-- >>> distinctMax100 [1,2,3,2,1,101]
-- Empty
--
distinctMax100 :: [Int] -> Optional [Int]
distinctMax100 l =
  evalStateT (filtering test l) []
  where
  test :: Int -> StateT [Int] Optional Bool
  test x
    | x <= 100 = do
        seen <- get
        put (x:seen)
        pure (not (x `elem` seen))
    | otherwise = StateT $ \_ -> Empty


{- EXERCISE: OptionalT ; computations that can "fail" -}

data OptionalT k a =
  OptionalT { runOptionalT :: k (Optional a) }

instance (Functor k) => Functor (OptionalT k) where
  fmap :: (a -> b) -> OptionalT k a -> OptionalT k b
  fmap f ka = OptionalT $ fmap f <$> runOptionalT ka

instance (Applicative k) => Applicative (OptionalT k) where
  pure :: a -> OptionalT k a
  pure a = OptionalT $ pure (Full a)

  (<*>) :: OptionalT k (a -> b) -> OptionalT k a -> OptionalT k b
  kf <*> ka = OptionalT $
    liftA2 (<*>) (runOptionalT kf) (runOptionalT ka)

instance (Monad k) => Monad (OptionalT k) where
  (>>=) :: OptionalT k a -> (a -> OptionalT k b) -> OptionalT k b
  ka >>= f = OptionalT $ do
    o <- runOptionalT ka
    case o of
      Empty -> pure Empty
      Full a -> runOptionalT (f a)


-- | Remove all duplicate integers from a list. Produce a log as you
-- go.  If there is an element above 100, then abort the entire
-- computation and produce no result, but **always keep a log**.  If
-- you abort the computation, produce a log with the value,
-- "aborting > 100: " followed by the value that caused it.  If you
-- see an even number, produce a log message, "even number: "
-- followed by the even number.  Other numbers produce no log
-- message.
--
-- /Tip:/ Use `filtering` and `StateT [Int] (OptionalT (Writer [String]))`
--
-- >>> distinctMax100WithLog [1,2,3,2,6]
-- Writer ["even number: 2","even number: 2","even number: 6"] (Full [1,2,3,6])
--
-- >>> distinctMax100WithLog [1,2,3,2,6,106]
-- Writer ["even number: 2","even number: 2","even number: 6","aborting > 100: 106"] Empty
distinctMax100WithLog :: [Int] -> Writer [String] (Optional [Int])
distinctMax100WithLog l =
  runOptionalT (evalStateT (filtering test l) [])
  where
  test :: Int -> StateT [Int] (OptionalT (Writer [String])) Bool
  test x
    | x > 100 = liftS (failO (tell ["aborting > 100: " <> show x]))
    | even x = do
        liftS (liftO (tell ["even number: " <> show x]))
        nextState x
    | otherwise = nextState x
  nextState x = do
    seen <- get
    put (x:seen)
    pure (not (x `elem` seen))

  {- equivalent to:

  test x = StateT $ \s -> OptionalT $ case () of
    _ | x > 100 -> tell ["aborting > 100: " <> show x] $> Empty
      | even x -> tell ["even number: " <> show x] $> nextState x s
      | otherwise -> pure (nextState x s)
  nextState x seen = Full (x `notElem` seen , x:seen)

  -}


{- EXERCISE: "lifting" functions

Implement the following functions, then use them to refactor
'distinctMax100WithLog' above.

Neither of the two approaches - explicit constructors versus lifting
functions - is necessarily better or worse.  The approaches may even
be mixed.  Use whichever approach you prefer.

-}

-- lift a computation into the StateT
liftS :: (Functor k) => k a -> StateT s k a
liftS ka = StateT $ \s -> (\a -> (a, s)) <$> ka

-- lift a computation into an OptionalT
liftO :: (Functor k) => k a -> OptionalT k a
liftO ka = OptionalT $ Full <$> ka

-- lift a computation into an /Empty/ OptionalT (failure)
failO :: (Functor k) => k a -> OptionalT k b
failO ka = OptionalT $ Empty <$ ka


{- EXERCISE: WriterT

Writer has a monad transformer.

Note the isomorphism between 'Writer w a' (as initially defined
above) and '(a, w)'.  The definition of 'WriterT' uses the latter
representation.

-}

data WriterT w k a = WriterT { runWriterT :: k (a, w) }

instance (Show (k (a, w))) => Show (WriterT w k a) where
  show (WriterT k) = show k

instance (Functor k) => Functor (WriterT w k) where
  fmap :: (a -> b) -> WriterT w k a -> WriterT w k b
  fmap = error "todo"

instance (Monoid w, Applicative k) => Applicative (WriterT w k) where
  pure :: a -> WriterT w k a
  pure = error "todo"

  (<*>) :: WriterT w k (a -> b) -> WriterT w k a -> WriterT w k b
  (<*>) = error "todo"

instance (Monoid w, Monad k) => Monad (WriterT w k) where
  (>>=) :: WriterT w k a -> (a -> WriterT w k b) -> WriterT w k b
  (>>=) = error "todo"

tell' :: (Applicative k) => w -> WriterT w k ()
tell' = error "todo"

-- replace the old definition of 'Writer' with:
-- type Writer w = WriterT w Identity
--
-- and then you can delete the Writer instances, etc.
